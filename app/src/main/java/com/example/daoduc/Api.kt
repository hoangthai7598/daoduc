package com.example.daoduc

import com.example.daoduc.model.Listener
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

const val BASE_URL = "http://27.71.232.170:81/data/"

interface Api {
    @GET("data.json")
    fun getMusic(
    ): Call<Listener>

    companion object {
        operator fun invoke() : Api{
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Api::class.java)
        }
    }
}