package com.example.daoduc

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Environment.getExternalStoragePublicDirectory
import android.os.Handler
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.daoduc.Database.SQLHelper
import kotlinx.android.synthetic.main.activity_main2.*
import java.io.File
import java.io.IOException
import java.util.concurrent.TimeUnit


class Main2Activity : AppCompatActivity() {
    private   var mediaPlayer: MediaPlayer = MediaPlayer()
    private var iscolor = false

    private lateinit var mDb: SQLiteDatabase

    var handler: Handler = Handler()
    var runnable: Runnable = Runnable {  }
    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)


        //tao nut quay ve
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()?.setDisplayShowHomeEnabled(true);

        //tieu de
        val actionBar = supportActionBar
        actionBar!!.title = "Nền tảng đạo đức"

        //lay noi dung
        val ss:String = intent.getStringExtra("TieuDe")
        val s:String = intent.getStringExtra("NoiDung")
        val sss:String = intent.getStringExtra("LinkAmThanh")
        val pos:Int = intent.getIntExtra("position",0)
        val Id:String = intent.getStringExtra("ID")
        val linkfb:String = intent.getStringExtra("LinkFaceBook")
        val linkha:String = intent.getStringExtra("LinkHinhAnh")
        val bookmark:Int = intent.getIntExtra("BookMark",0)
        val ma:String = intent.getStringExtra("Ma")

        //tao tieu de
        txtTieuDe.setText(ss)
        txtNoiDung.setText(s)

        //lay anh nen
        val imgName = linkha.replace(".jpg","")// specify here your image name fetched from db
        val uri = "drawable/"+imgName
        val icon: Int = getResources().getIdentifier(uri, "drawable", getPackageName())
        if(icon!=0)
            frame.setBackgroundResource(icon)
        else
            frame.setBackgroundResource(R.drawable.img_1)
        linear.setAlpha(0.8F)

        //chinh co chu
        var a: Float = txtNoiDung.getTextSize()
        buttonPlus.setOnClickListener{
            if(a<=40)
            {
                val b:Float = a +1
                a=b
                setTextSize(b)
            }
            else
                Toast.makeText(this,"Cỡ chữ đã lớn nhất",Toast.LENGTH_LONG).show()
        }
        buttonMinus.setOnClickListener{
            if(a>=4){
                val c:Float= a - 1
                a = c
                setTextSize(c)
            }
            else
                Toast.makeText(this,"Cỡ chữ đã nhỏ nhất",Toast.LENGTH_LONG).show()
        }

        //nut quay len dau trang
        btnUp.setOnClickListener{
            scroll.scrollTo(0,0)
        }
        //mo browser
        btnShare.setOnClickListener{
            val browserIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse(linkfb))
            startActivity(browserIntent)
        }
        //cau hoi
        btnTl.setOnClickListener{
            val intent = Intent(this,CauHoiActivity::class.java)
            intent.putExtra("TieuDe",ss)
            intent.putExtra("NoiDung",s)
            intent.putExtra("LinkAmThanh",sss)
            intent.putExtra("position",pos)
            intent.putExtra("ID",Id)
            intent.putExtra("LinkFaceBook",linkfb)
            intent.putExtra("LinkHinhAnh",linkha)
            intent.putExtra("BookMark",bookmark)
            intent.putExtra("Ma",ma)
            val tsLong = System.currentTimeMillis()
            intent.putExtra("time",tsLong)
            startActivity(intent)
            if(mediaPlayer.isPlaying)
                mediaPlayer.pause()
        }





        //doc file mp3

        val sharedPref: SharedPreferences =
            this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val bg = sharedPref.getString(
            "Voice","Nam")


        val downloadedFile = File(Environment.getExternalStorageDirectory(), "Download/" + sss+".mp3")
        val name = downloadedFile.getPath()
        val downloadedFileTest = File(Environment.getExternalStorageDirectory(), "Download/b_002.mp3")
        val nameTest = downloadedFileTest.getPath()
        val downloadedFile1 = File(Environment.getExternalStorageDirectory(), "Download/" + sss+".wav")
        val name1 = downloadedFile1.getPath()
        //set ten bai
        Tittle.setText("Audio "+ss)

        if(downloadedFile.exists())
        {
            if(bg=="Nu"){
                mediaPlayer.setDataSource(name);
                mediaPlayer.prepare();
            }
            else{
                mediaPlayer.setDataSource(nameTest);
                mediaPlayer.prepare();
            }
            //chinh text time
            val time = java.lang.String.format(
                "%02d:%02d ",
                TimeUnit.MILLISECONDS.toMinutes(mediaPlayer.duration.toLong()),
                TimeUnit.MILLISECONDS.toSeconds(mediaPlayer.duration.toLong()) -  TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mediaPlayer.duration.toLong()))
            )
            duration.setText(time)
        }
        else if(downloadedFile1.exists())
        {
            mediaPlayer.setDataSource(name1);
            mediaPlayer.prepare();
            //chinh text time
            val time = java.lang.String.format(
                "%02d:%02d ",
                TimeUnit.MILLISECONDS.toMinutes(mediaPlayer.duration.toLong()),
                TimeUnit.MILLISECONDS.toSeconds(mediaPlayer.duration.toLong()) -  TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mediaPlayer.duration.toLong()))
            )
            duration.setText(time)
        }
        else
        {
            duration.setText("")
        }
        play.setOnClickListener{
            if(downloadedFile.exists()|| downloadedFile1.exists()){
                mediaPlayer.start()
                playCycle()
                pause.setVisibility(View.VISIBLE)
                play.setVisibility(View.INVISIBLE)
            }
            else
            {
                Toast.makeText(this,"Tải bài nghe về để nghe",Toast.LENGTH_LONG).show()
                val intent = Intent(this,ListenActivity::class.java)
                startActivity(intent)
            }
        }
        pause.setOnClickListener{
            if(mediaPlayer.isPlaying())
            {
                mediaPlayer.pause()
                play.setVisibility(View.VISIBLE)
                pause.setVisibility(View.INVISIBLE)
            }
        }



        //tao progress bar
        handler = Handler()
        mediaPlayer.setOnPreparedListener {
            progress_bar.getProgressDrawable().setColorFilter(
                Color.GRAY, android.graphics.PorterDuff.Mode.SRC_IN);
            progress_bar1.getProgressDrawable().setColorFilter(
                Color.GREEN, android.graphics.PorterDuff.Mode.SRC_IN);
            progress_bar.setMax(mediaPlayer.duration)
            progress_bar1.setMax(mediaPlayer.duration)
        }

        //chinh yeu thich
        if(bookmark==0)
            btnLove.setImageResource(R.drawable.ic_favorite_white)
        else
            btnLove.setImageResource(R.drawable.ic_favorite_black)
        btnLove.setOnClickListener {
            if(bookmark==0) {
                btnLove.setImageResource(R.drawable.ic_favorite_black)
                readSQL()
            }
            else {
                btnLove.setImageResource(R.drawable.ic_favorite_white)
                readNoSQL()
            }
        }
    }
    //cap nhat yeu thich co
    fun readSQL()
    {
        val mDBHelper = SQLHelper(this)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.getReadableDatabase()
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val Id:String = intent.getStringExtra("ID")
        val cv = ContentValues()
        cv.put("Bookmark", 1)
        mDb.update ("DM_DeBai", cv, "ID = ?", arrayOf(Id))
    }
    //cap nhat yeu thich
    fun readNoSQL()
    {
        val mDBHelper = SQLHelper(this)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
            Log.d("lol",mIOException.toString())
        }
        try {
            mDb = mDBHelper.getReadableDatabase()
        } catch (mSQLException: SQLException) {
            throw mSQLException
            Log.d("lol",mSQLException.toString())
        }
        val Id:String = intent.getStringExtra("ID")
        val cv = ContentValues()
        cv.put("Bookmark", 0)
        mDb.update ("DM_DeBai", cv, "ID = ?", arrayOf(Id))
    }
    //chinh co chu
    fun setTextSize(size: Float) {
        txtNoiDung.setTextSize(TypedValue.COMPLEX_UNIT_SP, size)
    }
    override fun onResume() {
        super.onResume()
        val sharedPref: SharedPreferences =
            this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val bg = sharedPref.getInt(
            "background_resource",
            android.R.color.white
        ) // the second parameter will be fallback if the preference is not found
        val color = sharedPref.getString("color","")
        if(color=="White")
        {
            txtNoiDung.setBackgroundColor(Color.WHITE)
            txtNoiDung.setTextColor(Color.BLACK)
        }
        if(color == "Gray")
        {
            txtNoiDung.setBackgroundColor(Color.GRAY)
            txtNoiDung.setTextColor(Color.WHITE)
        }
        this.invalidateOptionsMenu()
    }
    override fun onStart() {
        super.onStart()
        val sharedPref: SharedPreferences =
            this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val bg = sharedPref.getInt(
            "background_resource",
            android.R.color.white
        ) // the second parameter will be fallback if the preference is not found
        val color = sharedPref.getString("color","")
        if(color=="White")
        {
            main2.setBackgroundColor(Color.WHITE)
            txtNoiDung.setBackgroundColor(Color.WHITE)
            txtNoiDung.setTextColor(Color.BLACK)
        }
        if(color == "Gray")
        {
            main2.setBackgroundColor(Color.GRAY)
            txtNoiDung.setBackgroundColor(Color.GRAY)
            txtNoiDung.setTextColor(Color.WHITE)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val sharedPref: SharedPreferences =
            this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val color = sharedPref.getString("color","")
        if(color=="White")
        {
            menu!!.findItem(R.id.color).setVisible(true)
            menu!!.findItem(R.id.color2).setVisible(false)
            txtNoiDung.setBackgroundColor(Color.WHITE)
            txtNoiDung.setTextColor(Color.BLACK)
        }
        if(color == "Gray")
        {
            menu!!.findItem(R.id.color).setVisible(false)
            menu!!.findItem(R.id.color2).setVisible(true)
            txtNoiDung.setBackgroundColor(Color.GRAY)
            txtNoiDung.setTextColor(Color.WHITE)
        }
        val menuItem = menu!!.findItem(R.id.color)
        menuItem.setOnMenuItemClickListener {
            val sharedPref: SharedPreferences = this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putInt("background_resource",Color.GRAY)
            editor.putInt("text-color",Color.WHITE)
            editor.putString("color","Gray")
            editor.apply()
            txtNoiDung.setBackgroundColor(Color.GRAY)
            txtNoiDung.setTextColor(Color.WHITE)
            main2.setBackgroundColor(Color.GRAY)
            menu!!.findItem(R.id.color).setVisible(false)
            menu!!.findItem(R.id.color2).setVisible(true)
            return@setOnMenuItemClickListener true
        }
        val menuItem1 = menu!!.findItem(R.id.color2)
        menuItem1.setOnMenuItemClickListener {
            val sharedPref: SharedPreferences = this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putInt("background_resource",Color.WHITE)
            editor.putString("color","White")
            editor.putInt("text-color",Color.BLACK)
            editor.apply()
            main.setBackgroundColor(Color.WHITE)
            main2.setBackgroundColor(Color.WHITE)
            menu!!.findItem(R.id.color).setVisible(true)
            menu!!.findItem(R.id.color2).setVisible(false)
            txtNoiDung.setBackgroundColor(Color.WHITE)
            txtNoiDung.setTextColor(Color.BLACK)
            return@setOnMenuItemClickListener true
        }
        return super.onCreateOptionsMenu(menu)
    }

    //chay am thanh
    fun playCycle() {
        progress_bar.setProgress(mediaPlayer.getCurrentPosition())
        progress_bar1.setProgress(mediaPlayer.getCurrentPosition())
        val time = java.lang.String.format(
            "%02d:%02d ",
            TimeUnit.MILLISECONDS.toMinutes(mediaPlayer.duration.toLong()-mediaPlayer.getCurrentPosition().toLong()),
            TimeUnit.MILLISECONDS.toSeconds(mediaPlayer.duration.toLong()-mediaPlayer.getCurrentPosition().toLong()) -  TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mediaPlayer.duration.toLong()-mediaPlayer.getCurrentPosition().toLong()))
        )
        duration.setText(time)
        if (mediaPlayer.isPlaying)
        {
            runnable =  Runnable{
                playCycle()
            }
            handler.postDelayed(runnable, 1000)
        }
        else
        {
            play.setVisibility(View.VISIBLE)
            pause.setVisibility(View.INVISIBLE)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.release()
        handler.removeCallbacks(runnable)
    }

}
