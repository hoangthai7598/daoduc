package com.example.daoduc.model


import com.google.gson.annotations.SerializedName

data class Listener(
    @SerializedName("baseURL")
    val baseURL: String,
    @SerializedName("data")
    val `data`: MutableList<Data> = mutableListOf(),
    @SerializedName("lessionMax")
    val lessionMax: Int
)
data class Data(
    @SerializedName("codeLesson")
    val codeLesson: String,
    @SerializedName("dataFile")
    val dataFile: String,
    @SerializedName("fileExtension")
    val fileExtension: String,
    @SerializedName("idLesson")
    val idLesson: String,
    @SerializedName("linkAudio")
    val linkAudio: String,
    @SerializedName("stt")
    val stt: Int,
    @SerializedName("titleLesson")
    val titleLesson: String
)