package com.example.daoduc.model

import com.google.gson.annotations.SerializedName


data class DMDeBai(
    @SerializedName("ID")
    var ID:  String = "",
    @SerializedName("MA")
    var MA: String = "",
    @SerializedName("IDChuDe")
    var IDChuDe:  String = "",
    @SerializedName("TenChuDe")
    var TenChuDe:  String = "",
    @SerializedName("TieuDe")
    var TieuDe:  String = "",
    @SerializedName("NoiDung")
    var NoiDung:  String = "",
    @SerializedName("LinkAmThanh")
    var LinkAmThanh:  String = "",
    @SerializedName("LinkHinhAnh")
    var LinkHinhAnh:  String = "",
    @SerializedName("GhiChu")
    var GhiChu:  String = "",
    @SerializedName("Bookmark")
    var Bookmark: Int,
    @SerializedName("SoDapAnDung")
    var SoDapAnDung: Int ,
    @SerializedName("IsActive")
    var IsActive: Int ,
    @SerializedName("LinkFaceBook")
    var LinkFaceBook: String = "",
    @SerializedName("SoLanTraLoiSai")
    var SoLanTraLoiSai: Int,
    @SerializedName("LockedAnswers")
    var LockedAnswers: Int

)