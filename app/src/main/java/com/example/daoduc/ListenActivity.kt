package com.example.daoduc

import android.Manifest
import android.app.DownloadManager
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.daoduc.Adapter.MusicAdapter
import com.example.daoduc.model.Listener
import com.example.testing.RetrofitClient
import kotlinx.android.synthetic.main.activity_listen.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListenActivity : AppCompatActivity() {

    private val STORAGE_PERMISSION_CODE: Int = 1000;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listen)

        //tao tieu de
        val actionBar = supportActionBar
        actionBar!!.title = "Danh sách bài nghe"

        //tao nut quay ve
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()?.setDisplayShowHomeEnabled(true);


        //download
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M){
            if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)==
                    PackageManager.PERMISSION_DENIED){
                //permission denied,request it

                //show popup for runtime permission
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),STORAGE_PERMISSION_CODE)
            }
            else{
                //permission already granted, perform download
            }
        }
        else{
            //system os in less than marshmallow, runtime permission not required ,perform download
        }

        fetchMusic()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            STORAGE_PERMISSION_CODE -> {
                if(grantResults.isNotEmpty() && grantResults[0]==
                        PackageManager.PERMISSION_GRANTED){
                    //permission granted

                }
                else{
                    //permission from popup was denied,show eror message
                    Toast.makeText(this,"Permission denied!",Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    fun fetchMusic(){

        RetrofitClient.instance.getMusic()
            .enqueue(object: Callback<Listener> {
                override fun onFailure(call: Call<Listener>, t: Throwable) {
                }

                override fun onResponse(
                    call: Call<Listener>,
                    response: Response<Listener>
                ) {
                    if(response.isSuccessful) {
                        val movies = response.body()
                        movies?.let {
                            showMusic(it)
                        }
                    }
                    else
                    {
                    }
                }

            })

        //         tao kieu 1
//        Api().getMusic().enqueue(object: Callback<Listener>{
//            override fun onFailure(call: Call<Listener>, t: Throwable) {
//
//            }
//
//            override fun onResponse(call: Call<Listener>, response: Response<Listener>) {
//                val musics = response.body()
//
//                musics?.let {
//                    showMusic(it)
//                }
//            }
//        })
    }
    private fun showMusic(movies: Listener) {
        recyclerview.layoutManager = LinearLayoutManager(this@ListenActivity)
        recyclerview.adapter = MusicAdapter(movies)
    }
}
