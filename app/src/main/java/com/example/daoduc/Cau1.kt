package com.example.daoduc

import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.widget.CompoundButtonCompat
import androidx.fragment.app.Fragment
import com.example.daoduc.Database.SQLHelper
import kotlinx.android.synthetic.main.cau1.*
import java.io.IOException


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class Cau1 : Fragment(){

    private lateinit var mDb: SQLiteDatabase
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        //khai bao va set cau hoi
        val view: View = inflater.inflate(R.layout.cau1,container,false) //pass the correct layout name for the fragment
        val text = view.findViewById<View>(R.id.Hoi) as TextView
        val cau1 = view.findViewById<View>(R.id.dapan1) as TextView
        val cau2 = view.findViewById<View>(R.id.dapan2) as TextView
        val cau3 = view.findViewById<View>(R.id.dapan3) as TextView
        val cau4 = view.findViewById<View>(R.id.dapan4) as TextView
        text.text = "Câu 1: "+readSQL()
        cau1.text = "A."+Cau1()
        cau2.text = "B."+Cau2()
        cau3.text = "C."+Cau3()
        cau4.text = "D."+Cau4()

        //kiem tra
        val cb1:CheckBox = view.findViewById<View>(R.id.cb1) as CheckBox
        val cb2:CheckBox = view.findViewById<View>(R.id.cb2) as CheckBox
        val cb3:CheckBox = view.findViewById<View>(R.id.cb3) as CheckBox
        val cb4:CheckBox = view.findViewById<View>(R.id.cb4) as CheckBox

        val ln1:LinearLayout = view.findViewById<View>(R.id.ln1) as LinearLayout
        val ln2:LinearLayout = view.findViewById<View>(R.id.ln2) as LinearLayout
        val ln3:LinearLayout = view.findViewById<View>(R.id.ln3) as LinearLayout
        val ln4:LinearLayout = view.findViewById<View>(R.id.ln4) as LinearLayout

        //tat ca click
        ln1.setOnClickListener{
            updateSQL("A")
            cb1.isChecked=true
            cb2.isChecked=false
            cb3.isChecked=false
            cb4.isChecked=false
        }
        ln2.setOnClickListener{
            updateSQL("B")
            cb1.isChecked=false
            cb2.isChecked=true
            cb3.isChecked=false
            cb4.isChecked=false
        }
        ln3.setOnClickListener{
            updateSQL("C")
            cb1.isChecked=false
            cb2.isChecked=false
            cb3.isChecked=true
            cb4.isChecked=false
        }
        ln4.setOnClickListener{
            updateSQL("D")
            cb1.isChecked=false
            cb2.isChecked=false
            cb3.isChecked=false
            cb4.isChecked=true
        }

        //checkbox click
        cb1.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                updateSQL("A")
                cb2.isChecked=false
                cb3.isChecked=false
                cb4.isChecked=false
            }
        }
        cb2.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                updateSQL("B")
                cb1.isChecked=false
                cb3.isChecked=false
                cb4.isChecked=false
            }
        }
        cb3.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                updateSQL("C")
                cb2.isChecked=false
                cb1.isChecked=false
                cb4.isChecked=false
            }
        }
        cb4.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                updateSQL("D")
                cb2.isChecked=false
                cb3.isChecked=false
                cb1.isChecked=false
            }
        }

        //khoa dap an
        if(DapAnNguoiDung()==DapAnDung())
        {
            ln1.isEnabled = false
            ln2.isEnabled = false
            ln3.isEnabled = false
            ln4.isEnabled = false
            cb1.isEnabled = false
            cb2.isEnabled = false
            cb3.isEnabled = false
            cb4.isEnabled = false

            if(DapAnDung()=="A")
            {
                val darkStateList =
                    ContextCompat.getColorStateList(context!!, R.color.checkbox_tinit_dark_theme)
                CompoundButtonCompat.setButtonTintList(cb1, darkStateList)
                cb1.isChecked=true
            }
            if(DapAnDung()=="B")
            {
                val darkStateList =
                    ContextCompat.getColorStateList(context!!, R.color.checkbox_tinit_dark_theme)
                CompoundButtonCompat.setButtonTintList(cb2, darkStateList)
                cb2.isChecked=true
            }
            if(DapAnDung()=="C")
            {
                val darkStateList =
                    ContextCompat.getColorStateList(context!!, R.color.checkbox_tinit_dark_theme)
                CompoundButtonCompat.setButtonTintList(cb3, darkStateList)
                cb3.isChecked=true
            }
            if(DapAnDung()=="D")
            {
                val darkStateList =
                    ContextCompat.getColorStateList(context!!, R.color.checkbox_tinit_dark_theme)
                CompoundButtonCompat.setButtonTintList(cb4, darkStateList)
                cb4.isChecked=true
            }
        }

        return view
    }

    fun  readSQL():String
    {

        val mDBHelper = SQLHelper(this.activity!!.applicationContext)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.readableDatabase
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val a:String = this.activity!!.intent.getStringExtra("ID")
        val b = 1
        val  c: Cursor = mDb . query ("DM_CauHoiDapAn", null, "IDDeBai = ? and STT = ?" ,
            arrayOf(a,b.toString()), null, null, null)
        var CauHoi:String = " "
        if(c.moveToFirst()) {
            do {
                CauHoi = c.getString(5)
            } while (c.moveToNext())
        }
        return CauHoi
    }
    fun  Cau1():String
    {

        val mDBHelper = SQLHelper(this.activity!!.applicationContext)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.readableDatabase
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val a:String = activity!!.intent.getStringExtra("ID")
        val b = 1
        val  c: Cursor = mDb . query ("DM_CauHoiDapAn", null, "IDDeBai = ? and STT = ?" ,
            arrayOf(a,b.toString()), null, null, null)
        var DapAn1:String = " "
        if(c.moveToFirst()) {
            do {
                DapAn1 = c.getString(6)
            } while (c.moveToNext())
        }
        return DapAn1
    }
    fun  Cau2():String
    {

        val mDBHelper = SQLHelper(this.activity!!.applicationContext)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.readableDatabase
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val a:String = activity!!.intent.getStringExtra("ID")
        val b = 1
        val  c: Cursor = mDb . query ("DM_CauHoiDapAn", null, "IDDeBai = ? and STT = ?" ,
            arrayOf(a,b.toString()), null, null, null)
        var DapAn2:String = " "
        if(c.moveToFirst()) {
            do {
                DapAn2 = c.getString(7)
            } while (c.moveToNext())
        }
        return DapAn2
    }
    fun  Cau3():String
    {
        val mDBHelper = SQLHelper(this.activity!!.applicationContext)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.readableDatabase
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val a:String = activity!!.intent.getStringExtra("ID")
        val b = 1
        val  c: Cursor = mDb . query ("DM_CauHoiDapAn", null, "IDDeBai = ? and STT = ?" ,
            arrayOf(a,b.toString()), null, null, null)
        var DapAn3:String = " "
        if(c.moveToFirst()) {
            do {
                DapAn3 = c.getString(8)
            } while (c.moveToNext())
        }
        return DapAn3
    }
    fun  Cau4():String
    {
        val mDBHelper = SQLHelper(this.activity!!.applicationContext)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.readableDatabase
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val a:String = activity!!.intent.getStringExtra("ID")
        val b = 1
        val  c: Cursor = mDb . query ("DM_CauHoiDapAn", null, "IDDeBai = ? and STT = ?" ,
            arrayOf(a,b.toString()), null, null, null)
        var DapAn4:String = " "
        if(c.moveToFirst()) {
            do {
                DapAn4 = c.getString(9)
            } while (c.moveToNext())
        }
        return DapAn4
    }
    fun  DapAnNguoiDung():String
    {

        val mDBHelper = SQLHelper(this.activity!!.applicationContext)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.readableDatabase
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val a:String = activity!!.intent.getStringExtra("ID")
        val b = 1
        val  c: Cursor = mDb . query ("DM_CauHoiDapAn", null, "IDDeBai = ? and STT = ?" ,
            arrayOf(a,b.toString()), null, null, null)
        var DapAnNguoiDung:String = " "
        if(c.moveToFirst()) {
            do {
                DapAnNguoiDung = c.getString(15)+""
            } while (c.moveToNext())
        }
        return DapAnNguoiDung
    }
    fun  DapAnDung():String
    {
        val mDBHelper = SQLHelper(this.activity!!.applicationContext)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.readableDatabase
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val a:String = activity!!.intent.getStringExtra("ID")
        val b = 1
        val  c: Cursor = mDb . query ("DM_CauHoiDapAn", null, "IDDeBai = ? and STT = ?" ,
            arrayOf(a,b.toString()), null, null, null)
        var DapAnDung:String = " "
        if(c.moveToFirst()) {
            do {
                DapAnDung = c.getString(10)
            } while (c.moveToNext())
        }
        return DapAnDung
    }

    fun updateSQL(DapAn:String)
    {
        val mDBHelper = SQLHelper(this.activity!!.applicationContext)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.readableDatabase
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val Id:String = activity!!.intent.getStringExtra("ID")
        val stt = 1
        val cv = ContentValues()
        cv.put("DapAnNguoiDung", DapAn)
        mDb.update ("DM_CauHoiDapAn", cv, "IDDeBai = ? and STT = ?", arrayOf(Id,stt.toString()))
    }
    override fun onPause() {
        super.onPause()
        if (mDb.isOpen()) {
            mDb.close();
        }
    }

    override fun onStart() {
        super.onStart()
        val sharedPref: SharedPreferences =
            this.getActivity()!!.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val bg = sharedPref.getInt(
            "background_resource",
            android.R.color.white
        ) // the second parameter will be fallback if the preference is not found
        val color = sharedPref.getString("color","")
        if(color=="White")
        {
            Hoi.setTextColor(Color.BLACK)
            dapan1.setTextColor(Color.BLACK)
            dapan2.setTextColor(Color.BLACK)
            dapan3.setTextColor(Color.BLACK)
            dapan4.setTextColor(Color.BLACK)
        }
        if(color == "Gray")
        {
            Hoi.setTextColor(Color.WHITE)
            dapan1.setTextColor(Color.WHITE)
            dapan2.setTextColor(Color.WHITE)
            dapan3.setTextColor(Color.WHITE)
            dapan4.setTextColor(Color.WHITE)
        }
    }
}