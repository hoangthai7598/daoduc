package com.example.daoduc

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment


class SettingFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_setting,container,false) //pass the correct layout name for the fragment
        val txt = view.findViewById<View>(R.id.txt) as TextView
        txt.setOnClickListener {
            val intent = Intent(this.activity,ListenActivity::class.java)
            startActivity(intent)
        }
        return view
    }
}