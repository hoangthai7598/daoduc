package com.example.daoduc

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.daoduc.Database.SQLHelper
import kotlinx.android.synthetic.main.activity_kiem_tra.*
import java.io.IOException
import java.util.concurrent.TimeUnit


class KiemTraActivity : AppCompatActivity() {
    private lateinit var mDb: SQLiteDatabase
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kiem_tra)

        //tao nut quay ve
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()?.setDisplayShowHomeEnabled(true);

        //tieu de
        val actionBar = supportActionBar
        actionBar!!.title = "Kiểm tra kết quả"

        //Lam lai
        btnLamLai.setOnClickListener{
            goToCauHoi()
        }
        val sai:Int = 5-getProfilesCount()
        Dung.setText(getProfilesCount().toString())
        txtDung.setText(getProfilesCount().toString())
        txtSai.setText(sai.toString())
        if(getProfilesCount()==5)
        {
            btnHoanThanh.setVisibility(View.VISIBLE)
            btnLamLai.setVisibility(View.INVISIBLE)
            btnGoiY.setVisibility(View.INVISIBLE)
        }
        else
        {
            if(Sai()==0)
            {
                update(1)
            }
            else if(Sai()==1)
            {
                update(2)
            }
            else
            {
                update(3)
            }
        }
        if(Sai()==3)
        {
            btnGoiY.setVisibility(View.VISIBLE)
            btnLamLai.setVisibility(View.INVISIBLE)
        }
        txtSoLan.setText((Sai()).toString())


        btnGoiY.setOnClickListener{
            updateAll()
            goToCauHoi()
            Lock()
        }

        if(getLock()==1)
        {
            btnGoiY.setVisibility(View.INVISIBLE)
            btnLamLai.setVisibility(View.INVISIBLE)
            btnHoanThanh.setVisibility(View.VISIBLE)
        }


        btnHoanThanh.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            updateSQL()
            startActivity(intent)
            finish()
        }
        //time
        val a:Long = intent.getLongExtra("time",0)
        val tsLong = System.currentTimeMillis()
        val minus = tsLong-a
        val time = java.lang.String.format(
            "%02d:%02d ",
            TimeUnit.MILLISECONDS.toMinutes(minus),
            TimeUnit.MILLISECONDS.toSeconds(minus - TimeUnit.MINUTES.toSeconds(
                TimeUnit.MILLISECONDS.toMinutes(minus))
        ))
        txtTime.setText(time)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            goToCauHoi()
        }
        return super.onOptionsItemSelected(item)
    }
    //get number of profile
    fun getProfilesCount(): Int {
        val Id:String = intent.getStringExtra("ID")
        val countQuery = "Select ta.ID from DM_CauHoiDapAn ta  inner join DM_CauHoiDapAn ta2 where ta.ID=ta2.ID and ta.DapAnDung = ta2.DapAnNguoiDung and ta.IDDeBai = ?"
        val mDBHelper = SQLHelper(this)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.getReadableDatabase()
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val cursor: Cursor = mDb.rawQuery(countQuery, arrayOf(Id))
        val count: Int = cursor.getCount()
        cursor.close()
        return count
    }
    fun goToCauHoi(){
        val ss:String = intent.getStringExtra("TieuDe")
        val s:String = intent.getStringExtra("NoiDung")
        val sss:String = intent.getStringExtra("LinkAmThanh")
        val pos:Int = intent.getIntExtra("position",0)
        val Id:String = intent.getStringExtra("ID")
        val linkfb:String = intent.getStringExtra("LinkFaceBook")
        val linkha:String = intent.getStringExtra("LinkHinhAnh")
        val bookmark:Int = intent.getIntExtra("BookMark",0)
        val ma:String = intent.getStringExtra("Ma")
        //time
        val a:Long = intent.getLongExtra("time",0)
        val intent = Intent(this,CauHoiActivity::class.java)
        intent.putExtra("TieuDe",ss)
        intent.putExtra("NoiDung",s)
        intent.putExtra("LinkAmThanh",sss)
        intent.putExtra("position",pos)
        intent.putExtra("ID",Id)
        intent.putExtra("LinkFaceBook",linkfb)
        intent.putExtra("LinkHinhAnh",linkha)
        intent.putExtra("BookMark",bookmark)
        intent.putExtra("Ma",ma)
        intent.putExtra("time",a)
        startActivity(intent)
        finish()
    }
    //mo khoa
    fun updateSQL()
    {
        val mDBHelper = SQLHelper(this)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
            Log.d("lol",mIOException.toString())
        }
        try {
            mDb = mDBHelper.getReadableDatabase()
        } catch (mSQLException: SQLException) {
            throw mSQLException
            Log.d("lol",mSQLException.toString())
        }
        val Id:String = intent.getStringExtra("ID")
        val ma:String = intent.getStringExtra("Ma").replace("TOPIC","")
        val int:Int = ma.toInt()+1
        if(int <10 )
        {
            val maso:String = "TOPIC00"+int.toString()
            val cv = ContentValues()
            cv.put("IsActive", 1)
            mDb.update ("DM_DeBai", cv, "Ma = ?", arrayOf(maso))
        }
        if (int <100 && int >9)
        {
            val maso:String = "TOPIC0"+int.toString()
            val cv = ContentValues()
            cv.put("IsActive", 1)
            mDb.update ("DM_DeBai", cv, "Ma = ?", arrayOf(maso))
        }
        if(int >99)
        {
            val maso:String = "TOPIC"+int.toString()
            val cv = ContentValues()
            cv.put("IsActive", 1)
            mDb.update ("DM_DeBai", cv, "Ma = ?", arrayOf(maso))
        }
    }
    fun update(SoLan:Int)
    {
        val mDBHelper = SQLHelper(this.applicationContext)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.readableDatabase
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val Id:String = intent.getStringExtra("ID")
        val cv = ContentValues()
        cv.put("SoLanTraLoiSai", SoLan)
        mDb.update ("DM_DeBai", cv, "ID = ? ", arrayOf(Id))
    }
    fun  Sai():Int
    {
        val mDBHelper = SQLHelper(this.applicationContext)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.readableDatabase
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val a:String = intent.getStringExtra("ID")
        val  c: Cursor = mDb . query ("DM_DeBai", null, "ID = ?" ,
            arrayOf(a), null, null, null)
        var Sai:Int = 0
        if(c.moveToFirst()) {
            do {
                Sai = c.getInt(17)+0
            } while (c.moveToNext())
        }
        return Sai
    }
    //update Lock answer
    fun Lock()
    {
        val mDBHelper = SQLHelper(this.applicationContext)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.readableDatabase
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val Id:String = intent.getStringExtra("ID")
        val cv = ContentValues()
        cv.put("LockedAnswers", 1)
        mDb.update ("DM_DeBai", cv, "ID = ? ", arrayOf(Id))
    }
    //get Lock answer
    fun getLock():Int
    {
        val mDBHelper = SQLHelper(this.applicationContext)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.readableDatabase
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val a:String = intent.getStringExtra("ID")
        val  c: Cursor = mDb . query ("DM_DeBai", null, "ID = ?" ,
            arrayOf(a), null, null, null)
        var Lock:Int = 0
        if(c.moveToFirst()) {
            do {
                Lock = c.getInt(18)+0
            } while (c.moveToNext())
        }
        return Lock
    }
    fun  updateAll()
    {
        val mDBHelper = SQLHelper(this.applicationContext)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
        }
        try {
            mDb = mDBHelper.writableDatabase
        } catch (mSQLException: SQLException) {
            throw mSQLException
        }
        val a:String = intent.getStringExtra("ID")
        //cau 1
        val cau1=1
        val cau2=2
        val cau3=3
        val cau4=4
        val cau5=5
        var cv = ContentValues()
        val  c1: Cursor = mDb . query ("DM_CauHoiDapAn", null, "IDDeBai =  ? and STT = ?" ,
            arrayOf(a,cau1.toString()), null, null, null)
        if(c1.moveToFirst()) {
            do
            {
                cv.put("DapAnNguoiDung",c1.getString(10))
                mDb.update ("DM_CauHoiDapAn", cv, "IDDeBai = ? and STT = ?", arrayOf(a,cau1.toString()))
            }
            while (c1.moveToNext())
        }
        //cau 2
        val  c2: Cursor = mDb . query ("DM_CauHoiDapAn", null, "IDDeBai =  ? and STT = ?" ,
            arrayOf(a,cau2.toString()), null, null, null)
        if(c2.moveToFirst()) {
            do
            {
                cv.put("DapAnNguoiDung",c2.getString(10))
                mDb.update ("DM_CauHoiDapAn", cv, "IDDeBai = ? and STT = ?", arrayOf(a,cau2.toString()))
            }
            while (c2.moveToNext())
        }
        //cau 3
        val  c3: Cursor = mDb . query ("DM_CauHoiDapAn", null, "IDDeBai =  ? and STT = ?" ,
            arrayOf(a,cau3.toString()), null, null, null)
        if(c3.moveToFirst()) {
            do
            {
                cv.put("DapAnNguoiDung",c3.getString(10))
                mDb.update ("DM_CauHoiDapAn", cv, "IDDeBai = ? and STT = ?", arrayOf(a,cau3.toString()))
            }
            while (c3.moveToNext())
        }
        //cau 4
        val  c4: Cursor = mDb . query ("DM_CauHoiDapAn", null, "IDDeBai =  ? and STT = ?" ,
            arrayOf(a,cau4.toString()), null, null, null)
        if(c4.moveToFirst()) {
            do
            {
                cv.put("DapAnNguoiDung",c4.getString(10))
                mDb.update ("DM_CauHoiDapAn", cv, "IDDeBai = ? and STT = ?", arrayOf(a,cau4.toString()))
            }
            while (c4.moveToNext())
        }
        //cau 5
        val  c5: Cursor = mDb . query ("DM_CauHoiDapAn", null, "IDDeBai =  ? and STT = ?" ,
            arrayOf(a,cau5.toString()), null, null, null)
        if(c5.moveToFirst()) {
            do
            {
                cv.put("DapAnNguoiDung",c5.getString(10))
                mDb.update ("DM_CauHoiDapAn", cv, "IDDeBai = ? and STT = ?", arrayOf(a,cau5.toString()))
            }
            while (c5.moveToNext())
        }
    }

    override fun onPause() {
        super.onPause()
        if (mDb.isOpen()) {
            mDb.close();
        }
    }
    override fun onStart() {
        super.onStart()
        val sharedPref: SharedPreferences =
            this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val bg = sharedPref.getInt(
            "background_resource",
            android.R.color.white
        ) // the second parameter will be fallback if the preference is not found
        main.setBackgroundColor(bg)
        val color = sharedPref.getString("color","")

        if(color=="White")
        {
            txtKiemtra.setTextColor(Color.BLACK)
            SocauDung.setTextColor(Color.BLACK)
            SocauSai.setTextColor(Color.BLACK)
            ThoiGian.setTextColor(Color.BLACK)
            SoLan.setTextColor(Color.BLACK)
            KetQua.setTextColor(Color.BLACK)
            tyle.setTextColor(Color.BLACK)
        }
        if(color == "Gray")
        {
            txtKiemtra.setTextColor(Color.WHITE)
            SocauDung.setTextColor(Color.WHITE)
            SocauSai.setTextColor(Color.WHITE)
            ThoiGian.setTextColor(Color.WHITE)
            SoLan.setTextColor(Color.WHITE)
            KetQua.setTextColor(Color.WHITE)
            tyle.setTextColor(Color.WHITE)
        }
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val sharedPref: SharedPreferences =
            this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val color = sharedPref.getString("color","")

        if(color=="White")
        {
            menu!!.findItem(R.id.color).setVisible(true)
            menu!!.findItem(R.id.color2).setVisible(false)
        }
        if(color == "Gray")
        {
            menu!!.findItem(R.id.color).setVisible(false)
            menu!!.findItem(R.id.color2).setVisible(true)
        }

        val menuItem = menu!!.findItem(R.id.color)
        menuItem.setOnMenuItemClickListener {
            val sharedPref: SharedPreferences = this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putInt("background_resource", Color.GRAY)
            editor.putInt("text-color", Color.WHITE)
            editor.putString("color","Gray")
            editor.apply()
            main.setBackgroundColor(Color.GRAY)
            txtKiemtra.setTextColor(Color.WHITE)
            SocauDung.setTextColor(Color.WHITE)
            SocauSai.setTextColor(Color.WHITE)
            ThoiGian.setTextColor(Color.WHITE)
            SoLan.setTextColor(Color.WHITE)
            KetQua.setTextColor(Color.WHITE)
            tyle.setTextColor(Color.WHITE)
            menu!!.findItem(R.id.color).setVisible(false)
            menu!!.findItem(R.id.color2).setVisible(true)
            return@setOnMenuItemClickListener true
        }

        val menuItem1 = menu!!.findItem(R.id.color2)
        menuItem1.setOnMenuItemClickListener {
            val sharedPref: SharedPreferences = this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putInt("background_resource", Color.WHITE)
            editor.putString("color","White")
            editor.putInt("text-color", Color.BLACK)
            editor.apply()
            main.setBackgroundColor(Color.WHITE)
            txtKiemtra.setTextColor(Color.BLACK)
            SocauDung.setTextColor(Color.BLACK)
            SocauSai.setTextColor(Color.BLACK)
            ThoiGian.setTextColor(Color.BLACK)
            SoLan.setTextColor(Color.BLACK)
            KetQua.setTextColor(Color.BLACK)
            tyle.setTextColor(Color.BLACK)
            menu!!.findItem(R.id.color).setVisible(true)
            menu!!.findItem(R.id.color2).setVisible(false)


            return@setOnMenuItemClickListener true
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onResume() {
        super.onResume()
        this.invalidateOptionsMenu()
        val sharedPref: SharedPreferences =
            this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val bg = sharedPref.getInt(
            "background_resource",
            android.R.color.white
        )
        main.setBackgroundColor(bg)
        val color = sharedPref.getString("color","")

        if(color=="White")
        {
            txtKiemtra.setTextColor(Color.BLACK)
            SocauDung.setTextColor(Color.BLACK)
            SocauSai.setTextColor(Color.BLACK)
            ThoiGian.setTextColor(Color.BLACK)
            SoLan.setTextColor(Color.BLACK)
            KetQua.setTextColor(Color.BLACK)
            tyle.setTextColor(Color.BLACK)
        }
        if(color == "Gray")
        {
            txtKiemtra.setTextColor(Color.WHITE)
            SocauDung.setTextColor(Color.WHITE)
            SocauSai.setTextColor(Color.WHITE)
            ThoiGian.setTextColor(Color.WHITE)
            SoLan.setTextColor(Color.WHITE)
            KetQua.setTextColor(Color.WHITE)
            tyle.setTextColor(Color.WHITE)
        }
        this.invalidateOptionsMenu()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        goToCauHoi()
        finish()
    }
}
