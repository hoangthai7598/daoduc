package com.example.daoduc.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.daoduc.*

class PageAdapter(fm:FragmentManager) :FragmentPagerAdapter(fm){
    override fun getItem(position: Int): Fragment {
       when(position){
           0 -> {return Cau1()}
           1 -> {return Cau2() }
           2 -> {return Cau3()}
           3 -> {return Cau4()}
           4 -> {return Cau5()}
           else -> {return Cau1()}
       }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when(position){
            0 -> {return "Câu 1"}
            1 -> {return "Câu 2"}
            2 -> {return "Câu 3"}
            3 -> {return "Câu 4"}
            4 -> {return "Câu 5"}
        }
        return super.getPageTitle(position)
    }

    override fun getCount(): Int {
        return 5
    }
}