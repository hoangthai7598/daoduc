package com.example.daoduc.Adapter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.daoduc.Main2Activity
import com.example.daoduc.R
import com.example.daoduc.model.DMDeBai
import kotlinx.android.synthetic.main.item_users.view.*


class DataAdapter  (val data :List<DMDeBai>): RecyclerView.Adapter<DataAdapter.DataViewHolder>(){

    private lateinit var context : Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        context = parent.context;
        return DataViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_users, parent, false)
        )
        val sharedPref: SharedPreferences =
            context.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val color = sharedPref.getString("color","")
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        val datas = data[position]
        holder.view.TieuDe.text = datas.TieuDe
        holder.view.NoiDung.text = datas.NoiDung


        val sharedPref: SharedPreferences =
            context.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val color = sharedPref.getString("color","")

        if(color=="White")
        {
            holder.view.TieuDe.setTextColor(Color.BLACK)
            holder.view.NoiDung.setTextColor(Color.BLACK)
        }
        if(color == "Gray")
        {
            holder.view.TieuDe.setTextColor(Color.WHITE)
            holder.view.NoiDung.setTextColor(Color.WHITE)
        }

        val imgName = datas.LinkHinhAnh.replace(".jpg","") // specify here your image name fetched from db

        val uri = "drawable/"+imgName
        val icon: Int = context.getResources().getIdentifier(uri, "drawable", context.getPackageName())
        if(icon!=0)
            holder.view.image.setImageResource(icon)
        else
            holder.view.image.setImageResource(R.drawable.img_1)

        if (datas.IsActive==1)
        {
            holder.view.bookmark.setImageResource(R.drawable.ic_bookmark_black_24dp)
        }
        else  holder.view.bookmark.setImageResource(R.drawable.ic_bookmark_white_24dp)
        holder.view.setOnClickListener{
            if(datas.IsActive==1){
                val intent = Intent(context, Main2Activity::class.java)
                intent.putExtra("TieuDe",datas.TieuDe)
                intent.putExtra("NoiDung",datas.NoiDung)
                intent.putExtra("LinkAmThanh",datas.LinkAmThanh)
                intent.putExtra("position",position)
                intent.putExtra("ID",datas.ID)
                intent.putExtra("Ma",datas.MA)
                intent.putExtra("BookMark",datas.Bookmark)
                intent.putExtra("LinkFaceBook",datas.LinkFaceBook)
                intent.putExtra("LinkHinhAnh",datas.LinkHinhAnh)
                context.startActivity(intent)
            }
            else
                Toast.makeText(context,"Bạn cần hoàn thành bài học trước đó để mở khóa bài này!",Toast.LENGTH_LONG).show()
        }
    }

    class DataViewHolder(val view: View) : RecyclerView.ViewHolder(view)

}