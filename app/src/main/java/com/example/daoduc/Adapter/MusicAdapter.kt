package com.example.daoduc.Adapter

import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.daoduc.R
import com.example.daoduc.model.Listener
import kotlinx.android.synthetic.main.item_music.view.*
import java.io.File


class MusicAdapter(val datas : Listener) : RecyclerView.Adapter<MusicAdapter.DataViewHolder>() {

    private lateinit var context : Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        context = parent.context;
        return DataViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_music, parent, false)
        )
    }

    override fun getItemCount() = datas.data.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        val data = datas.data[position]

        val filename: String = data.linkAudio.substring(data.linkAudio.lastIndexOf('/') + 1)
        val downloadedFile = File(Environment.getExternalStorageDirectory(), "Download/"+filename)
        val name = downloadedFile.getName()
        if(filename == name && downloadedFile.exists()){
            holder.view.btnDelete.setVisibility(View.VISIBLE)
            holder.view.btnDownload.setVisibility(View.INVISIBLE)
        }
        else {
            holder.view.btnDelete.setVisibility(View.INVISIBLE)
            holder.view.btnDownload.setVisibility(View.VISIBLE)
        }


        holder.view.txtBai.text = data.titleLesson
        holder.view.txtDungLuong.text = data.dataFile

        //handler button Download click
        holder.view.btnDownload.setOnClickListener{
            startDownloading(data.linkAudio)
            Toast.makeText(context,"Đang tải file xuống",Toast.LENGTH_LONG).show()
            holder.view.btnDelete.setVisibility(View.VISIBLE)
            holder.view.btnDownload.setVisibility(View.INVISIBLE)
        }
        holder.view.btnDelete.setOnClickListener{
            delete(data.linkAudio)
            Toast.makeText(context,"Đã xóa audio "+data.titleLesson,Toast.LENGTH_LONG).show()
            holder.view.btnDelete.setVisibility(View.INVISIBLE)
            holder.view.btnDownload.setVisibility(View.VISIBLE)
        }
    }
    fun delete(url:String){
        val filename: String = url.substring(url.lastIndexOf('/') + 1)
        val downloadedFile = File(Environment.getExternalStorageDirectory(), "Download/"+filename)
        downloadedFile.delete()
    }
    fun startDownloading(url:String){
        //download request
        val filename: String = url.substring(url.lastIndexOf('/') + 1)
        val request = DownloadManager.Request(Uri.parse(url))
        //allow type of networks to download file(s) by default both are allowed
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
        request.setTitle("Tải xuống audio "+filename)
        request.setDescription("Đang tải file....")
        request.allowScanningByMediaScanner()
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,filename)


        //get download service, and enqueue file
        val manager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        manager.enqueue(request)
    }

    class DataViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}