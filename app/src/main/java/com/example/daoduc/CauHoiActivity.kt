package com.example.daoduc

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.example.daoduc.Adapter.PageAdapter
import kotlinx.android.synthetic.main.activity_cau_hoi.*

class CauHoiActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cau_hoi)
        val actionBar = supportActionBar
        actionBar!!.title = "Câu hỏi"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayShowHomeEnabled(true)
        main_tabs_pager.adapter = PageAdapter(supportFragmentManager)
        main_tabs.setupWithViewPager(main_tabs_pager)
        main_tabs.setSelectedTabIndicatorColor(Color.parseColor("#000000"));
        main_tabs.setSelectedTabIndicatorHeight((5 * getResources().getDisplayMetrics().density).toInt());
        main_tabs.setTabTextColors(Color.parseColor("#ffffff"), Color.parseColor("#000000"));

    }

    fun CreateView()
    {
        main_tabs_pager.adapter = PageAdapter(supportFragmentManager)
        main_tabs.setupWithViewPager(main_tabs_pager)
        main_tabs.setSelectedTabIndicatorColor(Color.parseColor("#000000"));
        main_tabs.setSelectedTabIndicatorHeight((5 * getResources().getDisplayMetrics().density).toInt());
        main_tabs.setTabTextColors(Color.parseColor("#ffffff"), Color.parseColor("#000000"));
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //nut home
        if(item.itemId == android.R.id.home){
            gotoMain2()
        }
        return super.onOptionsItemSelected(item)
    }
    fun gotoMain2(){
        val ss:String = intent.getStringExtra("TieuDe")
        val s:String = intent.getStringExtra("NoiDung")
        val sss:String = intent.getStringExtra("LinkAmThanh")
        val pos:Int = intent.getIntExtra("position",0)
        val Id:String = intent.getStringExtra("ID")
        val linkfb:String = intent.getStringExtra("LinkFaceBook")
        val linkha:String = intent.getStringExtra("LinkHinhAnh")
        val bookmark:Int = intent.getIntExtra("BookMark",0)
        val ma:String = intent.getStringExtra("Ma")
        val intent = Intent(this,Main2Activity::class.java)
        intent.putExtra("TieuDe",ss)
        intent.putExtra("NoiDung",s)
        intent.putExtra("LinkAmThanh",sss)
        intent.putExtra("position",pos)
        intent.putExtra("ID",Id)
        intent.putExtra("LinkFaceBook",linkfb)
        intent.putExtra("LinkHinhAnh",linkha)
        intent.putExtra("BookMark",bookmark)
        intent.putExtra("Ma",ma)
        startActivity(intent)
        finish()
    }

    override fun onStart() {
        val sharedPref: SharedPreferences =
            this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val color = sharedPref.getString("color","")
        if(color=="White")
        {
            main_tabs_pager.setBackgroundColor(Color.WHITE)
        }
        if(color == "Gray")
        {
            main_tabs_pager.setBackgroundColor(Color.GRAY)
        }
        super.onStart()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val sharedPref: SharedPreferences =
            this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val color = sharedPref.getString("color","")

        if(color=="White")
        {
            menu!!.findItem(R.id.color).setVisible(true)
            menu!!.findItem(R.id.color2).setVisible(false)
        }
        if(color == "Gray")
        {
            menu!!.findItem(R.id.color).setVisible(false)
            menu!!.findItem(R.id.color2).setVisible(true)
        }

        val menuItem = menu!!.findItem(R.id.color)
        menuItem.setOnMenuItemClickListener {
            val sharedPref: SharedPreferences = this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putInt("background_resource", Color.GRAY)
            editor.putInt("text-color", Color.WHITE)
            editor.putString("color","Gray")
            editor.apply()
            main_tabs_pager.setBackgroundColor(Color.GRAY)
            CreateView()
            menu!!.findItem(R.id.color).setVisible(false)
            menu!!.findItem(R.id.color2).setVisible(true)
            return@setOnMenuItemClickListener true
        }

        val menuItem1 = menu!!.findItem(R.id.color2)
        menuItem1.setOnMenuItemClickListener {
            val sharedPref: SharedPreferences = this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putInt("background_resource", Color.WHITE)
            editor.putString("color","White")
            editor.putInt("text-color", Color.BLACK)
            editor.apply()
            main_tabs_pager.setBackgroundColor(Color.WHITE)
            CreateView()
            menu!!.findItem(R.id.color).setVisible(true)
            menu!!.findItem(R.id.color2).setVisible(false)
            return@setOnMenuItemClickListener true
        }
        return super.onCreateOptionsMenu(menu)
    }
}
