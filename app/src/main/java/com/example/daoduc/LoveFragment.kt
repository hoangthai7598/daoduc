package com.example.daoduc

import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.daoduc.Adapter.DataAdapter
import com.example.daoduc.Database.SQLHelper
import com.example.daoduc.model.DMDeBai
import kotlinx.android.synthetic.main.fragment_home.*
import java.io.IOException

class LoveFragment : Fragment() {
    private lateinit var c: Cursor
    private val dataList :ArrayList<DMDeBai> = arrayListOf()
    private lateinit var userAdapter: DataAdapter
    private lateinit var mDb: SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_love, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userAdapter = DataAdapter(dataList)
        recyclerview.layoutManager = LinearLayoutManager(this.activity, LinearLayoutManager.VERTICAL, false)
        recyclerview.adapter = userAdapter
        readSQL()
    }

    fun readSQL()
    {
        val mDBHelper = SQLHelper(this.activity!!.applicationContext)
        try {
            mDBHelper.updateDataBase()
        } catch (mIOException: IOException) {
            throw Error("UnableToUpdateDatabase")
            Log.d("lol",mIOException.toString())
        }
        try {
            mDb = mDBHelper.getReadableDatabase()
        } catch (mSQLException: SQLException) {
            throw mSQLException
            Log.d("lol",mSQLException.toString())
        }
        val a:Int = 1;
        val  c:Cursor = mDb . query ("DM_DeBai", null, "Bookmark = ?", arrayOf(a.toString()), null, null, null)
        dataList.clear()
        if(c.moveToFirst()) {
            do {
                val  a = DMDeBai(c.getString(0),
                    c.getString(1),
                    c.getString(2),
                    c.getString(3),
                    c.getString(4),
                    c.getString(5  ),
                    c.getString(6),
                    c.getString(7),
                    c.getString(8),
                    c.getInt(9),
                    c.getInt(10),
                    c.getInt(15),
                    c.getString(16)+"",
                    c.getInt(17),
                    c.getInt(18)
                )
                dataList.add(a)
            } while (c.moveToNext())
            userAdapter.notifyDataSetChanged()
        }
    }
}