package com.example.daoduc


import android.content.Context
import android.content.SharedPreferences
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //tao tieu de
        val actionBar = supportActionBar
        actionBar!!.title = "Nền tảng đạo đức"

        //check man hinh luc ban dau
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().replace(
                R.id.fragment_container,
                HomeFragment()
            ).commit()
        }

        //set click navigation bottom
        navigation.setOnNavigationItemSelectedListener{
            var selectedFragment: Fragment? = null
            if(it.itemId == R.id.navigation_home)
            {
                selectedFragment = HomeFragment()
            }
           if(it.itemId == R.id.navigation_love)
           {
               selectedFragment = LoveFragment()
           }
            if(it.itemId == R.id.navigation_setting)
            {
                selectedFragment = SettingFragment()
            }
            if(it.itemId == R.id.navigation_read)
            {
                selectedFragment = ReadFragment()
            }
            if (selectedFragment != null) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    selectedFragment).commit()
            };
            return@setOnNavigationItemSelectedListener true
        }
    }
    override fun onStart() {
        super.onStart()
        //set mau
        val sharedPref: SharedPreferences =
            this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val bg = sharedPref.getInt(
            "background_resource",
            android.R.color.white
        )
        main.setBackgroundColor(bg)
    }

    override fun onResume() {
        super.onResume()
        if(navigation.selectedItemId==R.id.navigation_home)
        {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    HomeFragment()).commit()
        }
        if(navigation.selectedItemId==R.id.navigation_love)
        {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    LoveFragment()).commit()
        }
        if(navigation.selectedItemId==R.id.navigation_read)
        {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    ReadFragment()).commit()
        }
        if(navigation.selectedItemId==R.id.navigation_setting)
        {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    SettingFragment()).commit()
        }
        this.invalidateOptionsMenu()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val sharedPref: SharedPreferences =
            this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val color = sharedPref.getString("color","")

        if(color=="White")
        {
            menu!!.findItem(R.id.color).setVisible(true)
            menu!!.findItem(R.id.color2).setVisible(false)
        }
        if(color == "Gray")
        {
            menu!!.findItem(R.id.color).setVisible(false)
            menu!!.findItem(R.id.color2).setVisible(true)
        }

        val menuItem = menu!!.findItem(R.id.color)
        menuItem.setOnMenuItemClickListener {
            val sharedPref: SharedPreferences = this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putInt("background_resource",Color.GRAY)
            editor.putInt("text-color",Color.WHITE)
            editor.putString("color","Gray")
            editor.apply()
            main.setBackgroundColor(Color.GRAY)
            menu!!.findItem(R.id.color).setVisible(false)
            menu!!.findItem(R.id.color2).setVisible(true)

            if(navigation.selectedItemId==R.id.navigation_home)
            {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    HomeFragment()).commit()
            }
            if(navigation.selectedItemId==R.id.navigation_love)
            {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    LoveFragment()).commit()
            }
            if(navigation.selectedItemId==R.id.navigation_read)
            {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    ReadFragment()).commit()
            }
            if(navigation.selectedItemId==R.id.navigation_setting)
            {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    SettingFragment()).commit()
            }

            return@setOnMenuItemClickListener true
        }

        val menuItem1 = menu!!.findItem(R.id.color2)
        menuItem1.setOnMenuItemClickListener {
            val sharedPref: SharedPreferences = this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putInt("background_resource",Color.WHITE)
            editor.putString("color","White")
            editor.putInt("text-color",Color.BLACK)
            editor.apply()
            main.setBackgroundColor(Color.WHITE)
            menu!!.findItem(R.id.color).setVisible(true)
            menu!!.findItem(R.id.color2).setVisible(false)

            if(navigation.selectedItemId==R.id.navigation_home)
            {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    HomeFragment()).commit()
            }
            if(navigation.selectedItemId==R.id.navigation_love)
            {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    LoveFragment()).commit()
            }
            if(navigation.selectedItemId==R.id.navigation_read)
            {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    ReadFragment()).commit()
            }
            if(navigation.selectedItemId==R.id.navigation_setting)
            {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    SettingFragment()).commit()
            }

            return@setOnMenuItemClickListener true
        }
        val menuNam = menu!!.findItem(R.id.nam)
        menuNam.setOnMenuItemClickListener{
            val sharedPref: SharedPreferences = this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putString("Voice","Nam")
            Toast.makeText(this,"Nam",Toast.LENGTH_LONG).show()
            editor.apply()
            return@setOnMenuItemClickListener true
        }
        val menuNu = menu!!.findItem(R.id.nu)
        menuNu.setOnMenuItemClickListener{
            val sharedPref: SharedPreferences = this.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putString("Voice","Nu")
            Toast.makeText(this,"Nữ",Toast.LENGTH_LONG).show()
            editor.apply()
            return@setOnMenuItemClickListener true
        }
        return super.onCreateOptionsMenu(menu)
    }

}
